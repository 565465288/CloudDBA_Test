<?php
namespace app\index\controller;

use Rds\Request\V20140815\CreateDiagnosticReportRequest;
use Rds\Request\V20140815\DescribeDiagnosticReportListRequest;
use think\Config;
use think\console\command\make\Controller;

include "../extend/aliyun-openapi-php-sdk/aliyun-php-sdk-core/Config.php";

class Index extends Controller
{
    /**
     *  有坑 ： 2018年03月23日19:34:31  在 MySQL5.7 版本下 运行会 返回：The request processing has failed due to some unknown error, exception or failure.
     */

    private $clientProfile;
    private $client;

    /**
     * 地域ID：https://help.aliyun.com/document_detail/53289.html?spm=a2c4g.11186623.2.4.0Lb4jD
     */
    public function __construct()
    {
        parent::__construct();
        Config::load(APP_PATH . 'config/clouddba.php');
        $this->clientProfile = \DefaultProfile::getProfile(
            Config::get('RegionID'),           // 地域ID
            Config::get('AccessKeyId'),
            Config::get('AccessKeySecret')
        );
        $this->client = new \DefaultAcsClient($this->clientProfile);
    }

    // 对应 CreateDiagnosticReport 接口
    public function create()
    {
        $CreateDiagnosticReport = new CreateDiagnosticReportRequest();
        $CreateDiagnosticReport->setDBInstanceId(Config::get('DBInstanceId'));
        $CreateDiagnosticReport->setStartTime(date('Y-m-d', time() - 8 * 60 * 60 * 2));
        $CreateDiagnosticReport->setEndTime(date('Y-m-d', time() - 8 * 60 * 60));

        try {
            $response = $this->client->getAcsResponse($CreateDiagnosticReport);
            return json($response);
        } catch (ServerException $e) {
            print "Error: " . $e->getErrorCode() . " Message: " . $e->getMessage() . "\n";
        } catch (ClientException $e) {
            print "Error: " . $e->getErrorCode() . " Message: " . $e->getMessage() . "\n";
        }
    }


    // 对应 DescribeDiagnosticReportList 接口
    public function getList()
    {
        $DescribeDiagnosticReportList = new DescribeDiagnosticReportListRequest();
        $DescribeDiagnosticReportList->setDBInstanceId(Config::get('DBInstanceId'));
        try {
            $response = $this->client->getAcsResponse($DescribeDiagnosticReportList);
            return json($response);
        } catch (ServerException $e) {
            print "Error: " . $e->getErrorCode() . " Message: " . $e->getMessage() . "\n";
        } catch (ClientException $e) {
            print "Error: " . $e->getErrorCode() . " Message: " . $e->getMessage() . "\n";
        }
    }

}