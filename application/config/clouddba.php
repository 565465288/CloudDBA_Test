<?php
/**
 * 阿里云 Rds CloudDBA 配置
 */
use think\Env;
return [
    'RegionID' => Env::get('RegionID','cn-shenzhen'),
    'DBInstanceId'  => Env::get('DBInstanceId'),
    'AccessKeyId'   => Env::get('AccessKeyId'),
    'AccessKeySecret'   => Env::get('AccessKeySecret'),

    'CreateDiagnosticReport' => [
        'url'       => 'https://rds.aliyuncs.com/',
        'Action'    => 'CreateDiagnosticReport',

    ],
];