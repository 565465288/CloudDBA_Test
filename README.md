体验RDS CloudDBA 接口
===============

系统基于ThinkPHP5开发

使用阿里云SDK ：https://help.aliyun.com/document_detail/53111.html

---

使用说明：

.env.example 更名为 .env 把该配置的配置上就可以使用.

接口说明文档地址：https://help.aliyun.com/document_detail/64301.html

创建：
http://example.com/index/index/create

获取列表：
http://example.com/index/index/getList

配置文件：
/application/config/clouddba.php

SDK位置：/extend/aliyun-openapi-php-sdk
